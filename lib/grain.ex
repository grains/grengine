defmodule Grengine.Grain do



  def start_link() do
    Agent.start_link(fn -> %{}  end)
  end

  def get(grain, key) do
    Agent.get(grain, fn map -> map[key] end)
  end

  def set(grain, key, value) do
    Agent.update(grain, &Map.put(&1, key, value))
  end


  def get_values(grain) do
    Agent.get(grain, &(&1))
  end


  def to_string(_grain) do
    "{}"
  end

end
