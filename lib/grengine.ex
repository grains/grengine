defmodule Grengine do
  @moduledoc """
  Documentation for Grengine.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Grengine.hello
      :world

  """
  def hello do
    :world
  end
end
