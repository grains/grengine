defmodule Grengine.Conductor do

  use GenServer
  alias Grengine.Performance
  require Logger

  def start_link, do: GenServer.start_link(__MODULE__, [], name: __MODULE__)


  def perform do
    get_performances()
    |> Enum.reduce(%{}, fn perf, acc ->
      {name, val} = do_performance(perf)
      case name do
        nil -> acc
        _   -> Map.put(acc, name, val)
      end
    end)
  end


  def get_performances do
    # see https://github.com/paulanthonywilson/gproc_select_examples/blob/master/test/gproc_select_test.exs
    # matcher = :ets.fun2ms(fn x -> x end)
    matcher = [{:"_", [], [:"$$"]}]
    :gproc.select(matcher)
  end

  def do_performance(perf) do
    [{_n, _l, {:performance, name}}, _pid, :undefined] = perf
    values = Performance.get_values(name)
    case length( Map.keys(values)) do
      0  -> {nil, nil}
      _  -> {name, Performance.get_values(name)}
    end
  end




end
