defmodule Grengine.PerformanceSupervisor do
  use Supervisor

  require Logger

  def start_link do

    Logger.info "Starting PerformanceSupervisor"
    Supervisor.start_link(__MODULE__, [], name: :performance_supervisor)
  end

  def start_performance(name) do
    Supervisor.start_child(:performance_supervisor, [name])
  end

  def init(_) do
    children = [
      worker(Grengine.Performance, [])
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end
