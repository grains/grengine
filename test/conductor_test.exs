defmodule ConductorTest do

  use ExUnit.Case, async: true

  alias Grengine.{Conductor, Performance, PerformanceSupervisor}

  describe "finding all performances" do

    setup do
      PerformanceSupervisor.start_link
      PerformanceSupervisor.start_performance("foo")
      PerformanceSupervisor.start_performance("bar")
      Performance.set_values("foo", %{s1: 42})
      :ok

    end

    test "get all running performances" do
      foo = Conductor.get_performances()
      assert is_list(foo)
      assert length(foo) == 2
      # assert [{:n, :l, {:performance, "foo"}}] == foo
    end

    test "handle single performance" do
      values = Conductor.perform()
      assert is_map(values)
      assert length(Map.keys(values)) == 1
    end

  end



end
