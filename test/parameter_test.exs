defmodule ParameterTest do

  use ExUnit.Case, async: true

  alias Grengine.Parameter



  describe "populated Parameter" do

    setup do
        {:ok, parameter} = Parameter.start_link("foo")
        {:ok, parameter: parameter}
    end

    test "represents string", %{parameter: parameter} do
        assert Parameter.to_string(parameter) == "(name:foo: none)"
    end

    test "set a value", %{parameter: parameter} do
      Parameter.set_value(parameter, 42)
      assert Parameter.value(parameter) == 42
    end

    # test "set all values for the player", %{player: player} do
    #   values = %{foo: 1, bar: 2}
    #   Player.update_parameters(player, values)
    #   assert Player.get_values(player) == %{bar: 2, foo: 1}
    # end


  end

end
