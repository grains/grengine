defmodule GrainTest do

  use ExUnit.Case, async: true

  doctest Grengine.Grain
  alias Grengine.Grain


  describe "empty Grain" do

    setup do
        {:ok, grain} = Grain.start_link()
        {:ok, grain: grain}
    end

    test "represents string", %{grain: grain} do
        assert Grain.to_string(grain) == "{}"
    end


    test "get nonexistent parameter", %{grain: grain} do
      assert Grain.get(grain, :bar) == nil
    end

    test "set a value", %{grain: grain} do
      Grain.set(grain, :foo, 88)
      assert Grain.get(grain, :foo) == 88
    end

    test "get all values", %{grain: grain} do
      Grain.set(grain, :foo, 88)
      Grain.set(grain, :bar, 42)

      assert Grain.get_values(grain) == %{foo: 88, bar: 42}

    end

  end


end
